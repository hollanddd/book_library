// site/js/views/book.js

var app = app || {};

app.BookView = Backbone.View.extend({
  tagName: 'div',
	id: 'bookContainer',
	className: 'col-md-3',
	template: _.template( $('#bookTemplate' ).html() ),
	render: function() {
	  //this.el is defined in tagName. use $el to get access to jQuery html() function
		this.$el.html( this.template( this.model.toJSON() ) );
		return this;
	},
	events: { 'click #delete': 'deleteBook' },
	deleteBook: function(){
	  // delete model
	  this.model.destroy();
	  // delete view
	  this.remove();
	},
	parse: function( response ){
    response.id = response._id;
    return response;
  }
});
