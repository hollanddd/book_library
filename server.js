var application_root = __dirname,
  express = require('express'),
	path = require( 'path' ),
	mongoose = require( 'mongoose' );

var app = express();

// Config server
app.configure( function() {
	//parses request body and populates request.body
	app.use( express.bodyParser() );

	//checks request.body for HTTP method overrides
	app.use( express.methodOverride() );

	//perform route lookup based on url and HTTP method
	app.use(app.router);
	
	//Where to serve static content
	app.use( express.static( path.join( application_root, 'site' ) ) );

	//show all errors in development
	app.use( express.errorHandler( { dumpExceptions: true, showStack: true } ) );
});

var port = 4711;
app.listen( port, function() { 
	console.log( 'Express server listening on %d', port );
});

// Routes 
app.get( '/api', function(request, response) {
  response.send( 'Library API is running' );
});

app.get( '/api/books', function(request, response){
  return BookModel.find( function( err, books ){
    if(!err){
      return response.send(books);
    } else {
      return console.log( err );
    }
  });
});

// GETTING CRUDDY
// Creates book in db
app.post( '/api/books', function(request, response){
  var book = BookModel({
    title: request.body.title,
    author: request.body.author,
    releaseDate: request.body.releaseDate,
    keywords: request.body.keywords
  });
  book.save( function( err ){
    if(!err){
      return console.log('created');
    }else{
      return console.log(err);
    }
  });
    return response.send( book );
});

// Return single book from db
app.get( '/api/books/:id', function(request, response){
  return BookModel.findById(request.params.id, function(err, book){
    if(!err){
      return response.send(book);
    }else{
      return console.log(err);
    }
  });
});

// Update book in db
app.put( '/api/books/:id', function(request, response){
  console.log( 'Updating' + request.body.title );
  return BookModel.findById( request.params.id, function(err, book){
    book.title = request.body.title;
    book.author = request.body.author;
    book.releaseDate = request.body.releaseDate;
    keywords = request.body.keywords;
    
    return book.save( function( err ) {
      if( !err ) {
        console.log( 'book updated' );
      } else {
        console.log( err );
      }
      return response.send( book );
    });
  });
});

// Delete book in db
app.delete( '/api/books/:id', function(request, response) {
  console.log( 'Deleting book with id: ' + request.params.id );
  return BookModel.findById( request.params.id, function( err, book ) {
    return book.remove( function( err ){
      if( !err ) {
        console.log( 'Book Removed' );
        return response.send( '' );
      } else {
        console.log( err );
      }
    });
  });
});
// Connect to DataBase
mongoose.connect( 'mongodb://localhost/library_database' );

// Schemas
var Keywords = new mongoose.Schema({
  keyword: String
});

var Book = new mongoose.Schema({
  title: String,
  author: String,
  releaseDate: Date,
  keywords: [ Keywords ]
});

// Models
var BookModel = mongoose.model('Book', Book); 